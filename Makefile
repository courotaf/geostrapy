EDITOR = geany

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\033[93m"`\1`printf "\033[0m"`	\3/" | \
	expand -t20

open_all: # Open all projects files
	test -n ${VIRTUAL_ENV} && ${EDITOR} ${VIRTUAL_ENV}/bin/activate
	${EDITOR} .gitignore .gitlab-ci.yml Makefile README.md requirements.txt requirements-dev.txt setup.cfg
	${EDITOR} .git/hooks/p*-commit
	${EDITOR} cli/*.py

pre_commit: # Run the pre-commit hook
	.git/hooks/pre-commit

clean: # Remove files not tracked in source control
	rm -rf htmlcov .pytest_cache
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete
	find . -type d -empty -delete

lint: # Lint code
	${VIRTUAL_ENV}/bin/black --check --quiet cli/*.py
	${VIRTUAL_ENV}/bin/flake8 --config=setup.cfg
	${VIRTUAL_ENV}/bin/pydocstyle
	${VIRTUAL_ENV}/bin/pylint --rcfile=setup.cfg cli/*.py
