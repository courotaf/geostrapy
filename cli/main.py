#!/usr/bin/env python3
# coding:utf-8
"""Geostrapy CLI.

Share data related to physical activities over an API (geo-localized or not)
This module adds commands for `strava-cli` https://github.com/bwilczynski/strava-cli
"""
import click

from strava.commands import (
    login,
    logout,
    set_config,
)

from commands import post_create


@click.group()
def cli():
    """Launch the CLI."""


cli.add_command(login)
cli.add_command(logout)
cli.add_command(post_create)
cli.add_command(set_config)

if __name__ == "__main__":
    cli()
