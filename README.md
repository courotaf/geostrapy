geostrapy
=========

A python CLI to share data related to physical activities over an API (geo-localized or not).


💡 Idea
-------

1. Use services like [Strava](https://www.strava.com) to follow my physical activities (any other service is welcome)
1. Keep control over the data I share: just what I need ([_proportionality & relevance_](https://www.cnil.fr/fr/cnil-direct/question/quels-sont-les-grands-principes-des-regles-de-protection-des-donnees))
1. Respect python scripting guidelines proposed by _Vincent Bernat_ ( [🇫🇷](https://vincent.bernat.ch/fr/blog/2019-script-python-durable) | [🇬🇧](https://vincent.bernat.ch/en/blog/2019-sustainable-python-script) ).


✨ Features
------------

These are imagined with _[Strava](https://developers.strava.com/docs/reference/) in mind_, but any other service will be nice too :

* [x] [Create activity](https://lab.frogg.it/fcode/geostrapy/-/issues/1)
* [ ] From a GPX track: [get characteristics](https://lab.frogg.it/fcode/geostrapy/-/issues/2)
    * [ ] and [create an activity](https://lab.frogg.it/fcode/geostrapy/-/issues/3)
* Sanitize a GPX track:
    * [ ] [crop start, end & stop points](https://lab.frogg.it/fcode/geostrapy/-/issues/4)
    * [ ] [change start-time](https://lab.frogg.it/fcode/geostrapy/-/issues/5)
* [ ] [Post modified GPX file as activity](https://lab.frogg.it/fcode/geostrapy/-/issues/6)
* [ ] [Process GPX multi-track file](https://lab.frogg.it/fcode/geostrapy/-/issues/7)
* and [maybe more](https://lab.frogg.it/fcode/geostrapy/-/boards)…


🚀 Quickstart
-------------

```bash
git clone git@lab.frogg.it:fcode/geostrapy.git && cd geostrapy
/<path>/<to>/python3.9 -m venv ~/.venvs/geostrapy
source ~/.venvs/geostrapy/bin/activate
pip install -r requirements.txt
python cli/run.py --help
```


🚧 Development
--------------

- Follow our [`git` conventions](https://forga.gitlab.io/process/fr/manuel/convention/git/) (🇫🇷 only)
- Built with `Python 3.9`
- Code linting with [`flake8`](https://pypi.org/project/flake8/), [`pylint`](https://pypi.org/project/pylint), [`black`](https://pypi.org/project/black) & [pydocstyle](https://pypi.org/project/pydocstyle/).
- Install development tools:
    * `pip install -r requirements-dev.txt`
- A `Makefile` with tools : run `make help` to have a look
- Use the `pre-commit` git hook, [CI will run it](.gitlab-ci.yaml)
    * `echo "make --no-print-directory lint" > .git/hooks/pre-commit`
    * `chmod u+x .git/hooks/pre-commit`


### 📌 Dependences

Details in [`requirements.txt`](requirements.txt) & [`requirements-dev.txt`](requirements-dev.txt)

- [`gpxpy`](https://github.com/tkrajina/gpxpy/#readme) : GPX file manipulation
- [`strava-cli`](https://github.com/bwilczynski/strava-cli#readme) : Use Strava's API on command line


### 🤝 Contributing

- Roadmap ➡️ [_project kanban_](https://lab.frogg.it/fcode/geostrapy/-/boards)
- Question, suggestion, issue ➡️ [_project issues_](https://lab.frogg.it/fcode/geostrapy/-/issues/new)
- Code submission  ➡️ [_project merge request_](https://lab.frogg.it/fcode/geostrapy/-/merge_requests/new)
