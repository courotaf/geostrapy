#!/usr/bin/env python3
# coding:utf-8
"""Strava's API helpers."""
from strava.api._helpers import client, url, json


def post_activity(xargs):
    """Post an activity creation.

    Activity creation is made only with parameters (no GPX file).
    """
    response = client.post(url=url("/activities"), data=xargs)

    if response.ok:
        result = json(response)
    else:
        result = response.json()

    return result
