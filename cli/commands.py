#!/usr/bin/env python3
# coding:utf-8
"""Manage CLI commands.

Each function describes a CLI command.
"""

import datetime

import click
from strava.decorators import (
    output_option,
    login_required,
    format_result,
    TableFormat,
    OutputType,
)
from strava.formatters import (
    humanize,
    apply_formatters,
)

from api import post_activity

_ACTIVITY_COLUMNS = ("key", "value")


@click.option(
    "-n",
    "--name",
    prompt=True,
    type=str,
    required=True,
    default="Activity",
    help="The name of the activity",
)
@click.option(
    "-a",
    "--activity_type",
    prompt=True,
    type=str,
    required=True,
    default="run",
    help="Type of activity",
)
@click.option(
    "-s",
    "--start",
    prompt=True,
    type=str,
    required=True,
    default=datetime.datetime.now().isoformat(),
    help="ISO 8601 formatted date time",
)
@click.option("-t", "--time", prompt=True, type=int, required=True, help="In seconds")
@click.option("-d", "--desc", prompt=True, type=str, help="Description of the activity")
@click.option("-D", "--dist", prompt=True, type=int, help="In meters")
@click.option(
    "-c",
    "--commute",
    prompt=True,
    type=int,
    default="0",
    prompt_required=False,
    help="Set to 1 to mark as commute",
)
@click.option(
    "-T",
    "--trainer",
    prompt=True,
    type=int,
    default="0",
    prompt_required=False,
    help="Set to 1 to mark as a trainer activity",
)
@click.option(
    "-h",
    "--hide",
    prompt=True,
    type=int,
    default="0",
    prompt_required=False,
    help="Set to true to mute activity",
)
@click.command("create")
@output_option()
@login_required
def post_create(**kwargs):
    """Create an activity (manually)."""
    xargs = {
        "name": kwargs["name"],
        "type": kwargs["activity_type"],
        "trainer": kwargs["trainer"],
        "commute": kwargs["commute"],
        "distance": kwargs["dist"],
        "description": kwargs["desc"],
        "elapsed_time": kwargs["time"],
        "hide_from_home": kwargs["hide"],
        "start_date_local": kwargs["start"],
    }
    result = post_activity(xargs)
    _format_upload(result, output=kwargs["output"])


@format_result(
    table_columns=_ACTIVITY_COLUMNS,
    show_table_headers=False,
    table_format=TableFormat.PLAIN,
)
def _format_upload(result, output=None):
    return result if output == OutputType.JSON.value else _as_table(result)


def _as_table(upload_result):
    def format_id(upload_id):
        return f"{upload_id}"

    def format_error(upload_error):
        return f"{upload_error}"

    def format_property(name):
        return click.style(f"{humanize(name)}:", bold=True)

    formatters = {
        "id": format_id,
        "errors": format_error,
    }

    basic_data = [
        {"key": format_property(k), "value": v}
        for k, v in apply_formatters(upload_result, formatters).items()
    ]

    return [*basic_data]
